<?php
    if(isset($_POST['method']) && $_POST['method']!='') {

        if($_POST['method'] == "quote"){

            $sub = "Thank you for your quote request!";
            $message = "Dear ". $_POST['name'] .",\n \nWe have received your request successfully. Thank you for your interest in our product line. \nWe should have your proposal ready for you soon but in the meantime if you have any questions or require additional information please don't hesitate to contact us directly.";
            $message .= "\n \nRaceBIBs Team\ninfo@racebibs.in \n+91 7200 923300";
            $headers = "From: info@racebibs.in";

            $toUS ="Team, we got a new quote request \n 
                    Service: ".$_POST['service']."
                    Name: ".$_POST['name']."
                    Email: ".$_POST['email']."
                    Mobile: ".$_POST['phone']."
                    Message: ".$_POST['message'];

            if (@mail($_POST['email'], $sub, $message, $headers) && 
                @mail("info@racebibs.in", "New quote request from ". $_POST['name'], $toUS, $headers)){
                
                    echo 'Thank you! We will get back to you shortly!';
            }else{
                echo 'failed';
            }

        }else if($_POST['method'] == "contact"){
            $sub = "Thank you for getting in touch!";
            $message = "Dear ". $_POST['name'] .",\n \nWe appreciate you for contacting us about \"".$_POST['subject']."\". One of our colleagues will get back to you shortly.";
            $message .= "\n \nRaceBIBs Team\ninfo@racebibs.in \n+91 7200 923300";
            $headers = "From: info@racebibs.in";

            $toUS ="Team, we got a new contact \n 
                    Name: ".$_POST['name']."
                    Email: ".$_POST['email']."
                    Subject: ".$_POST['subject']."
                    Message: ".$_POST['message'];

            if (@mail($_POST['email'], $sub, $message, $headers) && 
                @mail("info@racebibs.in", "New contact from ". $_POST['name'], $toUS, $headers)){
                
                    echo 'Thank you! We will get back to you shortly!';
            }else{
                echo 'failed';
            }
        }else{
            echo "Try again !!";
        }
    }
?>
